package com.example.demo.utils;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

public class MyException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6101404272919563560L;
	
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public class BadRequestException extends RuntimeException {

		/**
		 * 
		 */
		private static final long serialVersionUID = 9018676360793706460L;
	   //
	}
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public class ResourceNotFoundException extends RuntimeException {

		/**
		 * 
		 */
		private static final long serialVersionUID = 8664137200117342920L;
	   //
	}
}
