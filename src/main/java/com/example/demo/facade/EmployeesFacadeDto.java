package com.example.demo.facade;

import java.util.Date;

import com.example.demo.service.EmployeesServiceDto;

public class EmployeesFacadeDto {
	
	private	int empNo;
	
	private	Date birthDate;
	
	private	String firstName;
	
	private	String lastName;
	
	private	String gender;
	
	private	Date hireDate;
	
	public EmployeesFacadeDto(EmployeesServiceDto dto) {
		this.empNo = dto.getEmpNo();
		this.birthDate = dto.getBirthDate();
		this.firstName = dto.getFirstName();
		this.lastName = dto.getLast_name();
		this.gender = dto.getGender();
		this.hireDate = dto.getHireDate();
	}
	
	public String getLast_name() {
		return lastName;
	}
	public void setLast_name(String lastName) {
		this.lastName = lastName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public int getEmpNo() {
		return empNo;
	}
	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public Date getHireDate() {
		return hireDate;
	}
	public void setHireDate(Date hireDate) {
		this.hireDate = hireDate;
	}
	
	public String toString() {
		return "Nombre: " + firstName + ", Apellidos: " + lastName;
	}
}
