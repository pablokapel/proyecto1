package com.example.demo.facade;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.service.EmployeesService;
import com.example.demo.service.EmployeesServiceDto;

@Service
public class EmployeesFacadeImpl implements EmployeesFacade {
	
	@Autowired
	private EmployeesService service;
	List<EmployeesServiceDto> serviceDtoList;
	List<EmployeesFacadeDto> facadeDtoList;

	@Override
	public List<EmployeesFacadeDto> findDtoByFirstName(String name) {
		serviceDtoList = service.findDtoByFirstName(name);
		facadeDtoList = new ArrayList<EmployeesFacadeDto>();
		for(EmployeesServiceDto e : serviceDtoList)
			facadeDtoList.add(new EmployeesFacadeDto(e));
		return facadeDtoList;
	}

}
