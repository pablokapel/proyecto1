package com.example.demo.facade;

import java.util.List;

public interface EmployeesFacade {
	public List<EmployeesFacadeDto> findDtoByFirstName(String name);
}
