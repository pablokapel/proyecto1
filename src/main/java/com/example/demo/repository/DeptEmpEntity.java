package com.example.demo.repository;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "dept_emp")
public class DeptEmpEntity {
	
	@EmbeddedId
	private	DeptEmpEntityPK deptEmpPK;
	
	@Column(name = "from_date")
	private	Date fromDate;
	@Column(name = "to_date")
	private	Date toDate;
	
	public DeptEmpEntityPK getDeptEmpPK() {
		return deptEmpPK;
	}
	public void setDeptEmpPK(DeptEmpEntityPK deptEmpPK) {
		this.deptEmpPK = deptEmpPK;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	
	
}
