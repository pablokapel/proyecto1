package com.example.demo.repository;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "employees")
public class EmployeesEntity {
	@Id
	@Column(name = "emp_no", length = 10)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private	int empNo;
	@Column(name = "birth_date")
	private	Date birthDate;
	@Column(name = "first_name", length = 14)
	private	String firstName;
	@Column(name = "last_name", length = 16)
	private	String lastName;
	@Column(name = "gender", length = 1)
	private	String gender;
	@Column(name = "hire_date")
	private	Date hireDate;
	
	@OneToMany(mappedBy="deptEmpPK.employee",targetEntity=DeptEmpEntity.class)
	private Collection<DeptEmpEntity> deptEmp;
	
	public String getLast_name() {
		return lastName;
	}
	public void setLast_name(String lastName) {
		this.lastName = lastName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public int getEmpNo() {
		return empNo;
	}
	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public Date getHireDate() {
		return hireDate;
	}
	public void setHireDate(Date hireDate) {
		this.hireDate = hireDate;
	}
	
	public String getDepts() {
		String cadena = "El emplado (" + toString() + ") ha estado en los siguientes departamentos:";
		if(this.deptEmp.isEmpty()) {
			cadena += " No ha estado en ningún departamento.";
		}else {
			for(DeptEmpEntity e : this.deptEmp) {
				cadena += " ";
				cadena += e.getDeptEmpPK().getDepartament().getDeptNo();
			}
		}
		
		return cadena;
	}
	
	public String toString() {
		return "Nombre: " + firstName + ", Apellidos: " + lastName;
	}
	
}
