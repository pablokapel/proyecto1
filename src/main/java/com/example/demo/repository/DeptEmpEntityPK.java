package com.example.demo.repository;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Embeddable
public class DeptEmpEntityPK implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6243558559187347684L;

	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="emp_no", nullable = false)
    private EmployeesEntity employee;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="dept_no", nullable = false)
    private DepartamentsEntity department;
	
	public EmployeesEntity getEmployee() {
		return employee;
	}
	public void setEmployee(EmployeesEntity employee) {
		this.employee = employee;
	}

	public DepartamentsEntity getDepartament() {
		return department;
	}
	public void setDepartament(DepartamentsEntity departament) {
		this.department = departament;
	}
}
