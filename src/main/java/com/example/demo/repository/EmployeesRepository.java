package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeesRepository extends JpaRepository<EmployeesEntity, Integer>{
	@Query("select e from EmployeesEntity e where e.firstName = :firstName")
	List<EmployeesEntity> findByFirstName(@Param("firstName") String firstName);
	@Query("select e from EmployeesEntity e where e.empNo = :empNo")
	EmployeesEntity findByEmpNo(@Param("empNo") int empNo);
}
