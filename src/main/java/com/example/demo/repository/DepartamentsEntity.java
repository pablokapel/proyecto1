package com.example.demo.repository;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "departments")
public class DepartamentsEntity {
	@Id
	@Column(name = "dept_no", length = 4)
	private	String deptNo;
	@Column(name = "dept_name", length = 40)
	private	String deptName;
	
	@OneToMany(mappedBy="deptEmpPK.department",targetEntity=DeptEmpEntity.class)
	private Collection<DeptEmpEntity> deptEmp;
	
	public String getDeptName() {
		return deptName;
	}
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
	public String getDeptNo() {
		return deptNo;
	}
	public void setDeptNo(String deptNo) {
		this.deptNo = deptNo;
	}
	
	public String toString() {
		return "DeptNo: " + deptNo + ", DeptName: " + deptName + ". ";
	}
}
