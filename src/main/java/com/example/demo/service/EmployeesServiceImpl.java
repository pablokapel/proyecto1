package com.example.demo.service;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.repository.EmployeesEntity;
import com.example.demo.repository.EmployeesRepository;

@Service
public class EmployeesServiceImpl implements EmployeesService {

	
	@Autowired
	private EmployeesRepository repository;
	List<EmployeesEntity> entityList;
	List<EmployeesServiceDto> serviceDtoList;
	
	public List<EmployeesServiceDto> findDtoByFirstName(String name) {
		entityList = repository.findByFirstName(name);
		serviceDtoList = new ArrayList<EmployeesServiceDto>();
		for(EmployeesEntity e : entityList)
			serviceDtoList.add(new EmployeesServiceDto(e));
		return serviceDtoList;
	}
	
	public EmployeesServiceDto saveEmployee(EmployeesServiceDto employee) {
		EmployeesEntity entity = new EmployeesEntity();
		//entity.setEmpNo(employee.getEmpNo());
		entity.setFirstName(employee.getFirstName());
		entity.setLast_name(employee.getLast_name());
		entity.setGender(String.valueOf(employee.getGender()));
		entity.setBirthDate(Date.from(Instant.now()));
		entity.setHireDate(Date.from(Instant.now()));
		entity = repository.save(entity);
		return new EmployeesServiceDto(entity);
	}
	
}
