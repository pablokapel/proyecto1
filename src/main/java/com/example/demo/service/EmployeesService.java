package com.example.demo.service;

import java.util.List;

public interface EmployeesService {
	public List<EmployeesServiceDto> findDtoByFirstName(String name);
	public EmployeesServiceDto saveEmployee(EmployeesServiceDto employee);
}
