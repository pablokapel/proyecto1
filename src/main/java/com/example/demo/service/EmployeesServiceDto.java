package com.example.demo.service;

import java.util.Date;

import com.example.demo.repository.EmployeesEntity;

public class EmployeesServiceDto {
	
	private	int empNo;
	
	private	Date birthDate;
	
	private	String firstName;
	
	private	String lastName;
	
	private	String gender;
	
	private	Date hireDate;
	
	public EmployeesServiceDto() {
		
	}
	
	public EmployeesServiceDto(EmployeesEntity entity) {
		this.empNo = entity.getEmpNo();
		this.birthDate = entity.getBirthDate();
		this.firstName = entity.getFirstName();
		this.lastName = entity.getLast_name();
		this.gender = entity.getGender();
		this.hireDate = entity.getHireDate();
	}
	
	public String getLast_name() {
		return lastName;
	}
	public void setLast_name(String lastName) {
		this.lastName = lastName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public int getEmpNo() {
		return empNo;
	}
	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public Date getHireDate() {
		return hireDate;
	}
	public void setHireDate(Date hireDate) {
		this.hireDate = hireDate;
	}
	
	public String toString() {
		return "Nombre: " + firstName + ", Apellidos: " + lastName;
	}
}
