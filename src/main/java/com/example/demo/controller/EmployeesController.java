package com.example.demo.controller;

import java.util.List;

public interface EmployeesController {
	public List<EmployeesControllerDto> findDtoByFirstName(String name);
	public String saveEmployee(EmployeesRestDto employeesRestDto);
	public String getDeptByEmpNo(String empNo);
	public String saveEmpInDept(DeptEmpRestDto deptEmpDto);
}
