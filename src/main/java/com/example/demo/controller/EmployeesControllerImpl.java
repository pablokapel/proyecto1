package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.facade.EmployeesFacade;
import com.example.demo.facade.EmployeesFacadeDto;
import com.example.demo.repository.DepartamentsEntity;
import com.example.demo.repository.DepartmentRepository;
import com.example.demo.repository.DeptEmpEntity;
import com.example.demo.repository.DeptEmpEntityPK;
import com.example.demo.repository.DeptEmpRepository;
import com.example.demo.repository.EmployeesEntity;
import com.example.demo.repository.EmployeesRepository;
import com.example.demo.service.EmployeesService;
import com.example.demo.service.EmployeesServiceDto;

@RestController
@RequestMapping(path="/test")
public class EmployeesControllerImpl implements EmployeesController {
	
	@Autowired
	private EmployeesFacade facade;
	List<EmployeesFacadeDto> facadeDtoList;
	List<EmployeesControllerDto> controllerDtoList;
	
	/* TESTING */
	@Autowired
	EmployeesService service;
	@Autowired
	private EmployeesRepository empRepository;
	@Autowired
	private DeptEmpRepository deptEmpRepository;
	@Autowired
	private DepartmentRepository deptRepository;

	@Override
	@RequestMapping(method=RequestMethod.GET, path="/getEmp/{name}")
	public List<EmployeesControllerDto> findDtoByFirstName(@PathVariable(name = "name", required = true) String name) {
		facadeDtoList = facade.findDtoByFirstName(name);
		controllerDtoList = new ArrayList<EmployeesControllerDto>();
		for(EmployeesFacadeDto e : facadeDtoList)
			controllerDtoList.add(new EmployeesControllerDto(e));
		return controllerDtoList;
	}

	@Override
	@RequestMapping(method=RequestMethod.POST, path="/save/empl")
	public String saveEmployee(@RequestBody EmployeesRestDto employeesRestDto) {
		EmployeesServiceDto employeesServiceDto = new EmployeesServiceDto();
		//employeesServiceDto.setEmpNo(employeesRestDto.getEmpNo());
		employeesServiceDto.setFirstName(employeesRestDto.getFirstName());
		employeesServiceDto.setLast_name(employeesRestDto.getLastName());
		employeesServiceDto.setGender(employeesRestDto.getGender());
		EmployeesServiceDto saveEmployee = service.saveEmployee(employeesServiceDto);
		String result = "";
		if(saveEmployee!=null) {
			result = "guardado correcto --> Empleado: " + saveEmployee;
		}else {
			result = "error en guardado";
		}
		return result;
	}
	
	@Override
	@RequestMapping(method=RequestMethod.GET, path="/getDept/{empNo}")
	public String getDeptByEmpNo(@PathVariable(name = "empNo", required = true) String empNo) {
		EmployeesEntity empEntity = empRepository.findByEmpNo(Integer.parseInt(empNo));
		String depts = "no existe ese empleado";
		if(empEntity!=null) {
			depts = empEntity.getDepts();
		}
		return depts;
	}
	
	@Override
	@RequestMapping(method=RequestMethod.POST, path="/save/empInDept")
	public String saveEmpInDept(@RequestBody DeptEmpRestDto deptEmpDto) {
		DeptEmpEntity entity = new DeptEmpEntity();
		DeptEmpEntityPK deptEmpPK = new DeptEmpEntityPK();
		deptEmpPK.setEmployee(empRepository.findByEmpNo(deptEmpDto.getEmpNo()));
		Optional<DepartamentsEntity> findDept = deptRepository.findById(deptEmpDto.getDeptNo());
		deptEmpPK.setDepartament(findDept.get());
		entity.setDeptEmpPK(deptEmpPK);
		entity.setFromDate(deptEmpDto.getFromDate());
		entity.setToDate(deptEmpDto.getToDate());
		DeptEmpEntity saved = deptEmpRepository.save(entity);
		String result = "";
		if(saved!=null) {
			result = "guardado correcto";
		}else {
			result = "error en guardado";
		}
		return result;
	}

}
